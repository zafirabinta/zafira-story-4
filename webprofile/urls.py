from .views import *
from django.shortcuts import render
from django.urls import path

app_name = "webprofile"
urlpatterns = [
    path('', profile, name="profile"),
    path('potofolio/', portofolio, name="portofolio"),
]